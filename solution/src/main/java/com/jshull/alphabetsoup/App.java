package com.jshull.alphabetsoup;

import java.io.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
/**
 * Solution Submission to Alphabet Soup Challenge
 * Submitted to Enlighten for Metronome
 * Jason Hull, hull.js@gmail.com
 * 619.203.9793
 *
 */
public class App 
{
    private static int rowCount, columnCount;
    //determining search vectors [-1,-1] == top left diagonal, [0,1] == right
    private static int[] x = {-1, -1, -1, 0, 0, 1, 1, 1};
    private static int[] y = {-1, 0, 1, -1, 1, -1, 0, 1};
    private static int[] startPoint = new int[2];
    private static int[] endPoint = new int[2];

    //with a row and column value, search all vector directions
    private static String searchGrid(char[][] grid, int row, int column, String word) {
        String output = "";
        boolean firstLetterMatch = false;

        //see if first char of first word matches grid letter, if true update start point
        if (grid[row][column] == word.charAt(0)) {
            firstLetterMatch = true;
            startPoint[0] = row;
            startPoint[1] = column;
        }

        if (firstLetterMatch) {
            int wordLength = word.length();

            //check all directions, if a direction falls out of the grid, skip it
            for (int searchDirection = 0; searchDirection < 8; searchDirection++) {
                int searchedLetter;
                int rowDirection = row + x[searchDirection];
                int columnDirection = column + y[searchDirection];

                for (searchedLetter = 1; searchedLetter < wordLength; searchedLetter++) {
                    if (rowDirection >= rowCount || rowDirection < 0 || columnDirection >= columnCount || columnDirection < 0) {
                        break;
                    }
                    if (grid[rowDirection][columnDirection] != word.charAt(searchedLetter)) {
                        break;
                    }

                    //update end point after the word has been searched
                    if (searchedLetter == wordLength-1) {
                        endPoint[0] = rowDirection;
                        endPoint[1] = columnDirection;
                        output = word + " " + startPoint[0] + ":" + startPoint[1] + " " + endPoint[0] + ":" + endPoint[1]; 
                    }
                    rowDirection += x[searchDirection];
                    columnDirection += y[searchDirection];
                }
            }
        }
        return output;
    }
    
    private static void getWordPosition(char[][] grid, List<String> words) {

        for (String word: words) {
            for (int row = 0; row < rowCount; row++) {
                for (int col = 0; col < columnCount; col++) {
                    String output = "";
                    output = searchGrid(grid, row, col, word);
                    if (!output.isEmpty()) {
                        System.out.println(output);
                    }
                }
            }
        }
    }

    private static char[][] buildGrid(String gridString, int rowCount, int columnCount) {

        char[][] letterGrid = new char[rowCount][columnCount];

        int offset = 0;
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < columnCount; j++) {
                letterGrid[i][j] = gridString.charAt(offset++);
            }
        }
        return letterGrid;
    }


    public static void main(String[] args) throws URISyntaxException, IOException {

        //to allow multiple different boards to be used
        String valid = "false";
        String filename = "";

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("\n"+"Which board would you like to use ? "+"\n"+"[1] Custom 5x5"+"\n"+"[2] Large Board"+"\n"+"[3] Sample 3x3"+"\n"+"[4] Sample 5x5 "+"\n");
            String choice = reader.readLine();
            
            switch (choice) {
                case "1":
                    filename = "custom5x5.txt";
                    valid = "true";
                    break;
                
                case "2":
                    filename = "largeboard.txt";
                    valid = "true";
                    break;
                
                case "3": 
                    filename = "sample3x3.txt";
                    valid = "true";
                    break;
                
                case "4": 
                    filename = "sample5x5.txt";
                    valid = "true";
                    break;

                default:
                    System.out.println("thats not in the list, choose a number in the list (1, 2, 3, 4): ");
                    valid = "false";
                    break;
            }

        } while (valid != "true");

        System.out.println("You chose: "+filename+"\n");
        
        InputStream in = App.class.getClassLoader().getResourceAsStream(filename);
        Scanner sc = new Scanner(in);
        
        //print the file contents to the console and get (grid dimensions, grid, & wordlist)
        String line;
        ArrayList<String> lines = new ArrayList<>();
        while(sc.hasNextLine()) {
            line = sc.nextLine();
            lines.add(line);
            System.out.println(line);
        }
        sc.close();
        
        String gridString = "";
        List<String> wordList = new ArrayList<String>();
        for (int l = 0; l < lines.size(); l++) {
            //read first line of the file for grid dimensions
            if (l == 0) {
                String[] gridSizeLine = lines.get(l).split("x");
                rowCount = Integer.parseInt(gridSizeLine[0]);
                columnCount = Integer.parseInt(gridSizeLine[1]);
                continue;
            }

            //get the letter grid lines removing spaces and the list of words
            if (lines.get(l).contains(" ")) {
                String gridStringWithoutSpaces = lines.get(l).replaceAll(" ", "");
                gridString += gridStringWithoutSpaces;
            } else {
                wordList.add(lines.get(l));
            }
        }
        char[][] finalLetterGrid = buildGrid(gridString, rowCount, columnCount);
        getWordPosition(finalLetterGrid, wordList);
    }
}
