package com.jshull.alphabetsoup;

// import static org.junit.Assert.*;
// import java.io.*;
// import org.junit.*;
import org.junit.Assert;
import org.junit.Test;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;


/**
 * Unit tests for Alphabet Soup.
 */
public class AppTest 
{
    /**
     *  Input Choice File Test
     */
    
    @Test
    public void userInputTest() {
        ByteArrayInputStream inputStream = new ByteArrayInputStream("3\n4\n".getBytes());
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(byteArrayOutputStream);
        UserInputExample userInputExample = new UserInputExample(inputStream, ps);
        userInputExample.start();
        String outputText = byteArrayOutputStream.toString();
        String key = "output:";
        String output = outputText.substring(outputText.indexOf(key) + key.length()).trim();
        Assert.assertEquals(output, "7");
    }
}
