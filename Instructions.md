# Alphabet Soup Code Challenge - Metronome
## Jason Hull, 619.203.9793 - (hull.js@gmail.com)
### Submitted via GitLab:
> Fork of Enlighten Programming Challenge / alphabet-soup
---
 - Built using Maven and VS Code

Dependencies: 
* JUnit for testing
  * Tests - User Input with InputStream and PrintStream

Install
```
$ mvn install
```

Test
```
$ mvn clean test
```

Run
```
$ java -cp solution/target/solution-1.0-SNAPSHOT.jar com.jshull.alphabetsoup.App
```